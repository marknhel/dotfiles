inoremap ;imp import <--> from <++><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;func function <-->(<++>){<Enter><++><Enter>}<Esc>/<--><Enter>"_4cl
inoremap ;try try{<Enter><--><Enter>}catch(error){<Enter><++>}<Esc>/<--><Enter>"_4cl
inoremap ;$ $(<-->).<++>(<++>);<Esc>/<--><Enter>"_4cl
inoremap ;get $.get(<-->, function(data, status){<enter><++><enter>});<Esc>/<--><Enter>"_4cl
inoremap ;' '<-->'<++><Esc>/<--><Enter>"_4cl
inoremap ;" "<-->"<++><Esc>/<--><Enter>"_4cl
inoremap ;( (<-->)<++><Esc>/<--><Enter>"_4cl
inoremap ;) (<-->)<++><Esc>/<--><Enter>"_4cl
inoremap ;{ {<-->}<++><Esc>/<--><Enter>"_4cl
inoremap ;} {<-->}<++><Esc>/<--><Enter>"_4cl
