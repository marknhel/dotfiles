inoremap ;html <!DOCTYPE html><Enter><html lang="en"><Enter><head><Enter><title><++></title><Enter><meta charset="utf-8"><Enter><meta name="viewport" content="width=device-width, initial-scale=1.0"><Enter></head><Enter><body><Enter><++><Enter></body><Enter></html><Esc>gg/<++><Enter>"_c4l
inoremap ;style <style><Enter><-->{<Enter><++><Enter>}<Enter><++><Enter></style><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;link <link rel="<-->" type="<++>" href="<++>"><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;> <-->{<Enter><++><Enter>}<Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;h1 <h1><--></h1><Enter><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;h2 <h2><--></h2><Enter><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;h3 <h3><--></h3><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;h4 <h4><--></h4><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;h5 <h5><--></h5><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;h6 <h6><--></h6><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;a <a class="<-->" id="<++>" href="<++>"><++></a><Space><++><Esc>/<--><Enter>"_c4l
inoremap ;p <p id="<-->" class="<++>"><Enter><Tab><++><Enter><Del></p><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;br <br><Enter>
inoremap ;hr <hr><Enter>
inoremap ;title <title><--></title>><Space><Space><++><Esc>/<--><Enter>"_c4l
inoremap ;b <b><--></b><Space><++><Esc>/<--><Enter>"_c4l
inoremap ;i <i><--></i><Space><++><Esc>/<--><Enter>"_c4l
inoremap ;div <div id="<-->" class="<++>" ><Enter><++><Enter></div><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;img <img id="<-->" class="<++>" src="<++>" alt="<++>" width="<++>" height="<++>" ><Enter><++><Esc>/<--><Enter>"_c4l
inoremap ;span <span id="<-->" class="<++>" ><++></span> <++><Esc>/<--><Enter>"_c4l
inoremap ;pre <pre><--></pre> <++><Esc>/<--><Enter>"_c4l
inoremap ;strong <strong><--></strong><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;em <em><--></em><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;small <small><--></small><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;mark <mark><--></mark><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;del <del><--></del><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;ins <ins><--></ins><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;sub <sub><--></sub><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;sup <sup><--></sup><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;q <q><--></q><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;blockquote <blockquote><--></blockquote><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;abbr <abbr title="<-->"><++></abbr><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;address <address><Enter><--><Enter></address><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;cite <cite><--></cite><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;bdo <bdo dir="<-->"><++></bdo><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;code <code id="<-->" class="<++>"><Enter><++><Enter></code><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;comment <!--<Enter><--><Enter>--><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;ul <ul id="<-->" class="<++>" ><Enter><++><Enter></ul><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;ol <ol id="<-->" class="<++>" ><Enter><++><Enter></ol><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;li <li id="<-->" class="<++>" ><++></li><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;? <?php <--> ;?><Esc>/<--><Enter>"_4cl
inoremap ;fo <form id="<-->" class="<++>" action="<++>" method="<++>" ><Enter><++><Enter><Enter></form><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;fe <feildset id="<-->" class="<++>" ><Enter><++><Enter><Enter></fieldset><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;leg <legend id="<-->" class="<++>" ><++></legend><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;inp <input type="<-->" name="<++>" id="<++>" class="<++>"><++><Space><++><Esc>/<--><Enter>"_4cl
inoremap ;lab <label for="<-->" id="<++>" class="<++>" ><++></label><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;sec <section id="<-->" class="<++>"><Enter><++><Enter></section><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;main <main id="<-->" class="<++>"><Enter><++><Enter></main><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;art <article id="<-->" class="<++>"><Enter><++><Enter></article><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;foot <footer id="<-->" class="<++>"><Enter><++><Enter></footer><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;field <fieldset id="<-->" class="<++>"><Enter><++><Enter></fieldset><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;sel <select id="<-->" class="<++>"><Enter><++><Enter></select><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;opt <option id="<-->" class="<++>"><Enter><++><Enter></option><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;text <textarea id="<-->" class="<++>"><Enter><++><Enter></textarea><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;but <button id="<-->" class="<++>" ><++></button><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;fig <figure id="<-->" class="<++>"><Enter><++><Enter></figure><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;header <header id="<-->" class="<++>"><Enter><++><Enter></header><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;nav <nav id="<-->" class="<++>"><Enter><++><Enter></nav><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;figcap <figcaption id="<-->" class="<++>"><Enter><++><Enter></figcaption><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;cap <caption id="<-->" class="<++>"><Enter><++><Enter></caption><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;table <table id="<-->" class="<++>"><Enter><++><Enter></table><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;cap <caption id="<-->" class="<++>"><Enter><++><Enter></caption><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;tbody <tbody id="<-->" class="<++>"><Enter><++><Enter></tbody><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;thead <thead id="<-->" class="<++>" ><++></thead><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;th <th><--></th><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;td <td><--></td><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;tr <tr><--></tr><Enter><++><Esc>/<--><Enter>"_4cl
inoremap ;vid <video id="<-->" class="<++>" width="<++>" height="<++>"><Enter><source src="<++>" type="<++>" ><Enter></video><Enter><++><Esc>/<--><Enter>"_4cl
